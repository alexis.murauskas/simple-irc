﻿using System;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using Simple_IRC.CLI;
using SimpleIRC.Connection;
using SimpleIRC.Interfaces;
using SimpleIRC.Messages;
using SimpleIRC.ServerAPI;

namespace SimpleIRC.CLI
{
    public class ServerConsole : IConsole
    {
        private readonly ServerConnection Connection;
        private readonly ServerController Server;

        public ServerConsole(ServerConnection connection, ServerController server)
        {
            Connection = connection;
            Server = server;
        }


        public Continue Run(string input)
        {
            // Admin input
            Console.ForegroundColor = Colors.Admin;

            try
            {
                if (input != null)
                {
                    Console.WriteLine("> " + input);
                    var adminMessage = MessageParser.ParseAdminMessage(input);
                    ProcessAdminCmd(adminMessage);

                    if (adminMessage.AdminCommand == AdminCommand.QUIT)
                        return Continue.NO;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Admin command could not be parsed: {0}", e.Message);
                return Continue.YES;
            }


            try
            {
                Connection.CheckForPendingClients();
                var clientMessages = Connection.Read();

                if (clientMessages.Any())
                {
                    foreach (var m in clientMessages)
                        ProcessUserCmd(m);
                }
            }
            catch(SocketException e)
            {
                Console.WriteLine("Encountered network error: {0}", e.Message);
                return Continue.NO;
            }
            catch(IOException e)
            {
                Console.WriteLine("Encountered network error: {0}", e.Message);
                return Continue.NO;
            }
            catch(Exception e)
            {
                Console.WriteLine("Unable to process command: {0}", e);
            }

            return Continue.YES;
        }


        public void Dispose()
        {
            Connection.Disconnect();
        }


        public void ProcessAdminCmd(AdminMessage message)
        {
            if(message.AdminCommand == AdminCommand.START)
            {
                Console.WriteLine("Server listening for incoming messages...");
            }

            if (message.AdminCommand == AdminCommand.QUIT)
            {
                Console.WriteLine("Server shutting down...");
                var response = Server.Disconnect();
                Connection.Write(response);
            }

            if(message.AdminCommand == AdminCommand.CREATE)
            {
                Console.WriteLine("Channel " + message.Parameters.ToList()[0] + " created...");
                var response = Server.CreateChannel(message);
                Connection.Write(response);
            }

        }


        public void ProcessUserCmd(ConnectionFrame frame)
        {
            ServerMessage response = null;
            var message = frame.Message;

            switch (message.UserCommand) 
            {
                case UserCommand.CONNECT:
                    response = Server.Connect(message);
                    Connection.UpdateClientName(frame);
                    Console.WriteLine(message.Source.Nickname + " is now connected...");
                    break;

                case UserCommand.JOIN:
                    response = Server.SubscribeUser(message);
                    Console.WriteLine(message.Source.Nickname + " joined channel...");
                    break;

                case UserCommand.PART:
                    response = Server.UnsubscribeUser(message);
                    Console.WriteLine(message.Source.Nickname + " left channel...");
                    break;

                case UserCommand.QUIT:
                    response = Server.Disconnect(message);
                    Connection.Write(response);
                    Connection.DisconnectClient(frame);
                    Console.WriteLine(message.Source.Nickname + " disconnected...");
                    return;

                case UserCommand.TOPICS:
                    response = Server.GetTopics(message);
                    Console.WriteLine("Available topics requested...");
                    break;

                case UserCommand.NAMES:
                    response = Server.GetChannelClients(message);
                    Console.WriteLine("Channel clients requested...");
                    break;

                case UserCommand.MSG:
                    response = Server.ProcessUserPost(message);
                    Console.WriteLine("Processing incoming user message...");
                    break;
            }

            Connection.Write(response);
        }
    }
}
