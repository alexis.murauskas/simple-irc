﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simple_IRC.CLI
{
    public static class Colors
    {
        public const ConsoleColor Admin = ConsoleColor.Magenta;
        public const ConsoleColor Client = ConsoleColor.Cyan;
        public const ConsoleColor Msg = ConsoleColor.White;
        public const ConsoleColor Error = ConsoleColor.Gray;
    }
}
