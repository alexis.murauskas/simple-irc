﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using Simple_IRC.CLI;
using SimpleIRC.ClientAPI;
using SimpleIRC.Interfaces;
using SimpleIRC.Messages;
using SimpleIRC.TcpConnection;

namespace SimpleIRC.CLI
{
    public class ClientConsole : IConsole
    {
        private readonly ClientConnection Connection;
        private readonly ClientController Client;


        public ClientConsole(ClientConnection connection, ClientController client)
        {
            Connection = connection;
            Client = client;
        }


        public Continue Run(string input)
        {
            // Client input
            Console.ForegroundColor = Colors.Client;

            try
            {
                if (input != null)
                {
                    Console.WriteLine("> " + input);
                    ProcessUserCmd(input);
                    Connection.Write(input);
                }
            }
            catch(IOException)
            {
                Console.WriteLine("Unable to read from server, disconnecting...");
                return Continue.NO;
            }
            catch (SocketException)
            {
                Console.WriteLine("Unable to read from server, disconnecting...");
                return Continue.NO;
            }
            catch (Exception e)
            {
                Console.WriteLine("User command could not be processed: {0}", e.Source);
                return Continue.YES;
            }


            try
            {
                IEnumerable<ServerMessage> serverMessages = null;
                while (serverMessages == null)
                {
                    serverMessages = Connection.ClientRead();
                };

                foreach (var m in serverMessages)
                {
                    ProcessServerReply(m);
                    if (m.UserAction == UserCommand.QUIT)
                        return Continue.NO;
                }
            }
            catch(IOException)
            {
                Console.WriteLine("Unable to read from server, disconnecting...");
                Dispose();
                return Continue.NO;
            }
            catch (SocketException)
            {
                Console.WriteLine("Unable to read from server, disconnecting...");
                Dispose();
                return Continue.NO;
            }
            catch (Exception e)
            {
                Console.WriteLine("Unable to process server reply: {0}", e.Message);
            }

            return Continue.YES;
        }


        public void Dispose()
        {
            Connection.Disconnect();
        }


        public void ProcessUserCmd(string input)
        {
            var message = MessageParser.ParseUserMessage(input);

            if (message.UserCommand == UserCommand.JOIN)
            {
                Client.JoinChannel(message.Parameters.ToList()[(int)Param.TARGET]);
            }

            if (message.UserCommand == UserCommand.PART)
            {
                Client.LeaveChannel();
            }
        }


        public void ProcessServerReply(ServerMessage message)
        {
            var confirm = Client.GetClientName() + " " + message.UserAction?.ToString();

            if (message.ServerReply == ServerReply.RPL_MSG)
                Console.ForegroundColor = Colors.Msg;
            else
                Console.ForegroundColor = Colors.Admin;

            switch (message.UserAction)
            {
                case UserCommand.QUIT:
                    Dispose();
                    break;

                case UserCommand.NAMES:
                    Connection.Write(confirm + " " + Client.GetTopic());
                    break;

                case UserCommand.TOPICS:
                    Connection.Write(confirm);
                    break;
            }

            Console.WriteLine("< "+ message.Message);
            Console.ForegroundColor = Colors.Client;
        }
    }
}
