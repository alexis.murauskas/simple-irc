﻿using SimpleIRC.ChatClient;
using SimpleIRC.ChatServer;
using SimpleIRC.CLI;
using SimpleIRC.ClientAPI;
using SimpleIRC.Connection;
using SimpleIRC.ServerAPI;
using SimpleIRC.TcpConnection;
using System;
using System.Net;
using System.Net.Sockets;

namespace Simple_IRC
{
    class Program
    {
        public static int Port = 13000;
        public static IPAddress Ip;


        static void Main(string[] args)
        {
            var addr = GetLocalIPAddress();
            Ip = IPAddress.Parse(addr);
            
            if (args.Length > 0) 
            {
                // DI for server
                var listener = new TcpListener(Ip, Port);
                var serverConnection = new ServerConnection(listener);
                var server = new Server(new ServerName(addr));
                var serverController = new ServerController(server);
                var serverConsole = new ServerConsole(serverConnection, serverController);

                serverConsole.Run(addr + " START");
                serverConsole.Run(addr + " CREATE dog-park");
                serverConsole.Run(addr + " CREATE pet-store");

                while (serverConsole.Run(null) != Continue.NO) ;

                // serverConsole.Run(addr + " QUIT");
                serverConsole.Dispose();
            }
            else
            {
                // DI for client
                var tcpClient = new TcpClient(addr, Port);
                var clientConnection = new ClientConnection(tcpClient, "");
                clientConnection.UserName = new ClientName(addr, "Lula");
                var client = new Client(clientConnection.UserName);
                var clientController = new ClientController(client);
                var clientConsole = new ClientConsole(clientConnection, clientController);

                // Demo
                clientConsole.Run(addr + ":Lula CONNECT " + addr);
                clientConsole.Run(addr + ":Lula JOIN dog-park");
                clientConsole.Run(addr + ":Lula MSG dog-park woof!");
                clientConsole.Run(addr + ":Lula PART dog-park");
                clientConsole.Run(addr + ":Lula JOIN pet-store");
                clientConsole.Run(addr + ":Lula MSG pet-store toys!");

                // while (clientConsole.Run(null) != Continue.NO) ;

                clientConsole.Run(addr + ":Lula QUIT");
                clientConsole.Dispose();
            }

            Console.ForegroundColor = ConsoleColor.White;
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    Console.WriteLine("Local IPv4 Address: " + ip.ToString());
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
    }
}
