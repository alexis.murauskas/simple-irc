﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using SimpleIRC.ChatClient;
using SimpleIRC.Interfaces;
using SimpleIRC.Messages;
using SimpleIRC.TcpConnection;

namespace SimpleIRC.Connection
{
    public class ServerConnection : IConnection
    {
        private readonly TcpListener Listener;
        private readonly List<ClientConnection> Clients;


        public ServerConnection(TcpListener listener)
        {
            Listener = listener;
            Clients = new List<ClientConnection>();
            Listener.Start();
        }


        public string generateID() => Guid.NewGuid().ToString("X");


        public void Disconnect()
        {
            Listener.Stop();
        }


        public void DisconnectClient(ConnectionFrame frame)
        {
            try
            {
                DisconnectClient(Clients.Find(c => c.ID == frame.ID));
            }
            catch (Exception e)
            {
                Console.WriteLine("Unable to disconnect client: {0}", e.Message);
            }
        }


        public void DisconnectClient(ClientConnection client)
        {
                Clients.Remove(client);
        }


        public void UpdateClientName(ConnectionFrame frame)
        {
            try
            {
                var name = frame.Message.Source;
                Clients.Find(c => c.ID == frame.ID).UserName = new ClientName(name.Name, name.Nickname);
            }
            catch(Exception e)
            {
                Console.WriteLine("Unable to update client name: {0}", e.Message);
            }
        }


        public void CheckForPendingClients()
        {
            if (Listener.Pending())
            {
                var client = Listener.AcceptTcpClient();
                Clients.Add(new ClientConnection(client, generateID()));
            }
        }


        public IEnumerable<ConnectionFrame> Read()
        {
            var result = new List<ConnectionFrame>();
            foreach (var c in Clients)
                result.AddRange(ReadClient(c));

            return result;
        }


        public IEnumerable<ConnectionFrame> ReadClient(ClientConnection client)
        {
            var result = new List<ConnectionFrame>();
            try 
            { 
                var data = client.ServerRead();
                foreach (var d in data)
                    result.Add(new ConnectionFrame(client.ID, d));
            }
            catch(Exception)
            {
                DisconnectClient(client);
                Console.WriteLine("Unable to read from client, removing client {0}...", client.UserName.Name);
            }

            return result;
        }


        public void Write(ServerMessage message)
        {
            foreach (var r in message.Recipients)
            {
                ClientConnection client = null;
                try
                {
                    client = Clients.Find(c => c.UserName?.Name == r.Name);
                    client.Write(message.Stringify());
                }
                catch (Exception)
                {
                    DisconnectClient(client);
                    Console.WriteLine("Unable to write to client, removing client {0}...", r.Name);
                }
            }
        }
    }
}
