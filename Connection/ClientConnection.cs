﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using SimpleIRC.ChatClient;
using SimpleIRC.Interfaces;
using SimpleIRC.Messages;

namespace SimpleIRC.TcpConnection
{
    public class ClientConnection : IConnection
    {
        public const int BufferSize = 1000000;

        public readonly string ID;
        public ClientName UserName;

        private readonly TcpClient Client;
        private readonly NetworkStream Stream;


        public ClientConnection(TcpClient client, string id)
        {
            ID = id;
            UserName = null;
            Client = client;
            Stream = Client.GetStream();
        }


        public void Disconnect()
        {
            Stream.Close();
            Client.Close();
        }


        public void Write(string message)
        {
            var data = System.Text.Encoding.ASCII.GetBytes(">"+message);
            Stream.Write(data, 0, data.Length);
        }


        public IEnumerable<UserMessage> ServerRead()
        {
            var data = new byte[BufferSize];
            var bytes = Stream.Read(data, 0, data.Length);
            var output = System.Text.Encoding.ASCII
                .GetString(data, 0, bytes)
                .Split(">", StringSplitOptions.RemoveEmptyEntries);

            var messages = new List<UserMessage>();
            foreach (var m in output)
                messages.Add(MessageParser.ParseUserMessage(m));

            return messages;
        }

        public IEnumerable<ServerMessage> ClientRead()
        {
            byte[] data = new byte[BufferSize];
            int bytes = bytes = Stream.Read(data, 0, data.Length);

            var output = System.Text.Encoding.ASCII
                    .GetString(data, 0, bytes)
                    .Split(">", StringSplitOptions.RemoveEmptyEntries);

            var messages = new List<ServerMessage>();
            foreach (var m in output)
                messages.Add(MessageParser.ParseServerMessage(m));

            return messages;
        }
    }
}
