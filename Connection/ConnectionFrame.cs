﻿using SimpleIRC.Interfaces;
using SimpleIRC.Messages;

namespace SimpleIRC.Connection
{
    public class ConnectionFrame
    {
        public readonly string ID;
        public readonly UserMessage Message;

        public ConnectionFrame(string id, UserMessage message)
        {
            ID = id;
            Message = message;
        }
    }
}
