﻿using SimpleIRC.Interfaces;

namespace SimpleIRC.ChatClient
{
    public class ClientName : IName
    {
        public readonly string Name;
        public readonly string Nickname;

        public ClientName(string name, string nickname)
        {
            Name = name;
            Nickname = nickname;
        }

        string IName.Name => Name;
        string IName.Nickname => Nickname;
    }
}
