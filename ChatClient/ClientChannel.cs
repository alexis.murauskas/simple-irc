﻿using System.Collections.Generic;
using SimpleIRC.Interfaces;

namespace SimpleIRC.ChatClient
{
    public class ClientChannel : IChannel
    {
        public readonly string Topic;

        public ClientChannel(string topic)
        {
            Topic = topic;
        }

        string IChannel.Topic => Topic;
    }
}
