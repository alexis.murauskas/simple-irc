﻿using System;
using SimpleIRC.Interfaces;

namespace SimpleIRC.ChatClient
{
    public class Client : IClient
    {
        public readonly ClientName Name;
        private ClientChannel Channel;


        public Client(ClientName name)
        {
            Name = name;
            Channel = new ClientChannel("");
        }


        public string GetTopic()
        {
            return Channel.Topic;
        }

        public string GetName()
        {
            return Name.Name + ":" + Name.Nickname;
        }


        public string UpdateChannel(ClientChannel channel)
        {
            Channel = channel ?? throw new ArgumentNullException(nameof(channel));
            return Channel.Topic;
        }
    }
}
