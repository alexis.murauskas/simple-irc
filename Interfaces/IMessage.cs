﻿using System.Collections.Generic;

namespace SimpleIRC.Interfaces
{
    public interface IMessage
    {
        IName Source { get; }
        IEnumerable<string> Targets { get; }
        string Stringify();
    }
}
