﻿using SimpleIRC.CLI;
using System;
using System.Threading.Tasks;

namespace SimpleIRC.Interfaces
{
    public interface IConsole
    {
        Continue Run(string input);
    }
}
