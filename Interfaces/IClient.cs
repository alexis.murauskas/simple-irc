﻿using System.Threading.Tasks;
using SimpleIRC.ChatClient;

namespace SimpleIRC.Interfaces
{
    public interface IClient
    {
        string GetTopic();
        string GetName();
        string UpdateChannel(ClientChannel channel);
    }
}
