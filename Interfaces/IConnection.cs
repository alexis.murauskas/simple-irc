﻿namespace SimpleIRC.Interfaces
{
    public interface IConnection
    {
        void Disconnect();
    }
}
