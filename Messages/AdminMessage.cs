﻿using System.Collections.Generic;
using SimpleIRC.ChatServer;
using SimpleIRC.Interfaces;

namespace SimpleIRC.Messages
{
    public class AdminMessage : IMessage
    {
        public readonly ServerName ServerName;
        public readonly AdminCommand AdminCommand;
        public readonly IEnumerable<string> Parameters;


        public AdminMessage(ServerName serverName, AdminCommand adminCommand, IEnumerable<string> parameters)
        {
            ServerName = serverName;
            AdminCommand = adminCommand;
            Parameters = parameters;
        }


        public IName Source => ServerName;
        public IEnumerable<string> Targets => Parameters;


        public string Stringify()
        {
            var param = "";
            foreach (var p in Parameters)
                param += " " + p;

            return ServerName.Name
                + " " + AdminCommand.ToString()
                + param;
        }
    }
}
