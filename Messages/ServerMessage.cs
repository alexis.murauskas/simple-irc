﻿using System;
using System.Collections.Generic;
using SimpleIRC.ChatClient;
using SimpleIRC.ChatServer;
using SimpleIRC.Interfaces;
using System.Linq;

namespace SimpleIRC.Messages
{
    public class ServerMessage : IMessage
    {
        public readonly ServerName ServerName;
        public readonly IEnumerable<IName> Recipients;
        public readonly ServerReply ServerReply;
        public readonly UserCommand? UserAction;
        public readonly string Message;


        public ServerMessage
        (
            ServerName serverName,
            IEnumerable<IName> recipients,
            ServerReply serverReply, 
            UserCommand userAction, 
            string message
        )
        {
            ServerName = serverName;
            Recipients = recipients;
            ServerReply = serverReply;
            UserAction = userAction;
            Message = message;
        }


        public IName Source => ServerName;
        public IEnumerable<string> Targets => Recipients.ToList().ConvertAll(r => r.Name + ":" + r.Nickname + ",");


        public string Stringify()
        {
            var recipient = "";
            foreach (var r in Recipients)
                recipient += r.Name + ":" + r.Nickname + ",";

            recipient.Remove(recipient.Length - 1);

            return ServerName.Name
                + " " + recipient
                + " " + ServerReply.ToString()
                + " " + UserAction.ToString()
                + " " + Message;
        }
    }
}
