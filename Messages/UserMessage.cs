﻿using System;
using System.Collections.Generic;
using SimpleIRC.ChatClient;
using SimpleIRC.Interfaces;
using System.Linq;

namespace SimpleIRC.Messages
{
    public class UserMessage : IMessage
    {
        public readonly ClientName ClientName;
        public readonly UserCommand UserCommand;
        public readonly IEnumerable<string> Parameters;



        public UserMessage(ClientName clientName, UserCommand userCommand, IEnumerable<string> parameters)
        {
            ClientName = clientName;
            UserCommand = userCommand;
            Parameters = parameters;
        }


        public IName Source => ClientName;
        public IEnumerable<string> Targets => Parameters;


        public string Stringify()
        {
            var param = "";
            foreach (var p in Parameters)
                param += " " + p;

            return ClientName.Name + ":" + ClientName.Nickname
                + " " + UserCommand.ToString()
                + param;
        }
    }
}
