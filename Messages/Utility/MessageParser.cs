﻿using System;
using SimpleIRC.ChatServer;
using SimpleIRC.ChatClient;
using System.Collections.Generic;
using System.Linq;

namespace SimpleIRC.Messages
{
    public static class MessageParser
    {
        private static readonly string[] AdminCommands =
        {
            "START",
            "QUIT",
            "CREATE"
        };

        private static readonly string[] UserCommands = 
        {
            "CONNECT",
            "JOIN",
            "PART",
            "QUIT",
            "TOPICS",
            "NAMES",
            "MSG",
            "NONE"
        };

        private static readonly string[] ServerReplies =
        {
            "RPL_CONFIRM",
            "RPL_ERROR",
            "RPL_DISCONNECT",
            "RPL_MSG",
            "RPL_NAMES",
            "RPL_TOPICS"
        };


        // Field parsing

        public static bool IsFromClient(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var list = input.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            if (list[(int)MsgProp.SOURCE].Contains(":"))
                return true;

            return false;
        }


        public static AdminCommand ParseAdminCommand(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var index = Array.IndexOf(AdminCommands, input);

            if (index < AdminCommands.GetLowerBound(0))
                throw new ArgumentException(nameof(input));

            return (AdminCommand)index;
        }


        public static UserCommand ParseUserCommand(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var index = Array.IndexOf(UserCommands, input);

            if (index < UserCommands.GetLowerBound(0))
                throw new ArgumentException(nameof(input));

            return (UserCommand)index;
        }


        public static ServerReply ParseServerReply(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var index = Array.IndexOf(ServerReplies, input);

            if (index < ServerReplies.GetLowerBound(0))
                throw new ArgumentException(nameof(input));

            return (ServerReply)index;
        }


        public static ClientName ParseClientName(string input)
        {
            var names = input.Split(':', StringSplitOptions.RemoveEmptyEntries);
            return new ClientName(names[(int)Name.IP], names[(int)Name.NICK]);
        }


        public static string ParseTextMessage(IEnumerable<string> text)
        {
            var result = "";
            foreach (var word in text)
                result += word + " ";

            return result;
        }


        // Message parsing

        public static AdminMessage ParseAdminMessage(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var list = input.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            List<string> param = null;
            if (list.Length > (int)MsgProp.PARAMS)
                param = new List<string> { list[(int)MsgProp.PARAMS] };

            var message = new AdminMessage
            (
                new ServerName(list[(int)MsgProp.SOURCE]),
                ParseAdminCommand(list[(int)MsgProp.CMD]),
                param
            );

            return message;
        }


        public static UserMessage ParseUserMessage(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var list = input.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            var pos = (int)MsgProp.PARAMS;
            var param = list.ToList().GetRange(pos, list.Length - pos);

            var message = new UserMessage
            (
                ParseClientName(list[(int)MsgProp.SOURCE]),
                ParseUserCommand(list[(int)MsgProp.CMD]),
                param
            );

            return message;
        }


        public static ServerMessage ParseServerMessage(string input)
        {
            if (input == null)
                throw new ArgumentNullException(nameof(input));

            var list = input.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            var names = list[(int)SrvMsgProp.TARGET].Split(',', StringSplitOptions.RemoveEmptyEntries);
            var targets = new List<ClientName>();

            foreach (var n in names)
                targets.Add(ParseClientName(n));

            var command = ParseUserCommand(list[(int)SrvMsgProp.CMD]);
            var pos = (int)SrvMsgProp.MSG;
            var text = ParseTextMessage(list.ToList().GetRange(pos, list.Length - pos));

            var message = new ServerMessage
            (
                new ServerName(list[(int)SrvMsgProp.SOURCE]),
                targets,
                ParseServerReply(list[(int)SrvMsgProp.REPLY]),
                command,
                text
            );

            return message;
        }
    }
}
