﻿using System;
namespace SimpleIRC.Messages
{
    public enum MsgProp
    {
        SOURCE,
        CMD,
        PARAMS
    }

    public enum SrvMsgProp
    {
        SOURCE,
        TARGET,
        REPLY,
        CMD,
        MSG
    }

    public enum Name
    {
        IP,
        NICK
    }

    public enum Param
    {
        TARGET,
        POST
    }
}
