﻿namespace SimpleIRC.Messages
{
    public enum ServerReply
    {
        RPL_CONFIRM,
        RPL_ERROR,
        RPL_DISCONNECT,
        RPL_MSG,
        RPL_NAMES,
        RPL_TOPICS
    }
}
