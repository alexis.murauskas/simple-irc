﻿using System.Collections.Generic;

namespace SimpleIRC.Messages
{
    public static class ServerPost
    {
        public static string Connect(string name) => name + " CONNECTED";
        public static string Disconnect(string name) => name + " DISCONNECTED";


        public static string Topics(IEnumerable<string> topics)
        {
            var rv = "TOPICS";
            foreach (var t in topics)
                rv += "\n" + t;

            return rv;
        }


        public static string Clients(IEnumerable<string> clients)
        {
            var rv = "MEMBERS";
            foreach (var c in clients)
                rv += "\n" + c;

            return rv;
        }


        public static string ClientJoined(string name, string channel)
            => name + " JOINED " + channel;


        public static string ClientLeft(string name, string channel)
            => name + " LEFT " + channel;


        public static string ChannelCreated(string channel) 
            => "CHANNEL " + channel + " CREATED";
    }
}
