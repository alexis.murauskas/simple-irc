﻿namespace SimpleIRC.Messages
{
    public enum UserCommand
    {
        CONNECT,
        JOIN,
        PART,
        QUIT,
        TOPICS,
        NAMES,
        MSG,
        NONE
    }
}
