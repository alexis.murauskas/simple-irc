﻿using System;
using System.Collections.Generic;
using SimpleIRC.ChatClient;
using SimpleIRC.Interfaces;

namespace SimpleIRC.ChatServer
{
    public class Server : IServer
    {
        public readonly ServerName ServerName;
        private List<IName> Clients;
        private List<ServerChannel> Channels;


        public Server(ServerName name)
        {
            ServerName = name;
            Clients = new List<IName>();
            Channels = new List<ServerChannel>();
        }


        ServerName IServer.ServerName => ServerName;
        List<IName> IServer.Clients => Clients;

        public IEnumerable<string> StringifyChannels() => Channels.ConvertAll(c => c.Topic);
        public IEnumerable<string> StringifyClients() => Clients.ConvertAll(c => c.Nickname);
        public IEnumerable<string> StringifyClients(string topic)
        {
            if (topic == null)
                throw new ArgumentNullException(nameof(topic));

            var members = Channels.Find(c => c.Topic == topic).Members;
            return members.ConvertAll(m => m.Nickname);
        }


        public IEnumerable<IName> GetMembers(string topic) 
        {
            if (topic == null)
                throw new ArgumentNullException(nameof(topic));

            return Channels.Find(c => c.Topic == topic).Members;
        }


        public IEnumerable<string> AddClient(ClientName client)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            Clients.Add(client);
            return StringifyClients();
        }


        public IEnumerable<string> AddChannel(string topic)
        {
            if (topic == null)
                throw new ArgumentNullException(nameof(topic));

            var channel = new ServerChannel(topic);
            Channels.Add(channel);

            return StringifyChannels();
        }


        public IEnumerable<string> SubscribeClient(string topic, ClientName client)
        {
            if (topic == null || client == null)
                throw new ArgumentNullException();

            Channels.Find(c => c.Topic == topic).Members.Add(client);
            return StringifyClients(topic);
        }


        public IEnumerable<string> UnsubscribeClient(string topic, ClientName client)
        {
            if (topic == null || client == null)
                throw new ArgumentNullException();

            if (!Clients.Contains(client))
                return new List<string>();

            Channels.Find(c => c.Topic == topic).Members
                .RemoveAll(l => l.Name == client.Name);

            return StringifyClients(topic);
        }


        public IEnumerable<string> DisconnectClient(ClientName client)
        {
            if (client == null)
                throw new ArgumentNullException();

            Channels.ForEach(c => c.Members
                .RemoveAll(l => l.Name == client.Name));

            Clients.RemoveAll(c => c.Name == client.Name);

            return StringifyClients();
        }
    }
}
