﻿using SimpleIRC.Interfaces;

namespace SimpleIRC.ChatServer
{
    public class ServerName : IName
    {
        public readonly string Name;

        public ServerName(string name) => Name = name;
        string IName.Name => Name;
        string IName.Nickname => null;
    }
}
